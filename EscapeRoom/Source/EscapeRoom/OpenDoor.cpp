// Fill out your copyright notice in the Description page of Project Settings.


#include "OpenDoor.h"

UOpenDoor::UOpenDoor()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();
	ActorThatOpens = GetWorld()->GetFirstPlayerController()->GetPawn();
	Owner = GetOwner();
}

void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (PressurePlate && PressurePlate->IsOverlappingActor(ActorThatOpens)) {
		OpenCloseDoor(true);
		LastDoorOpenTime = GetWorld()->GetTimeSeconds();
	}
	if (GetWorld()->GetTimeSeconds() - LastDoorOpenTime > DoorCloseDelay) {
		OpenCloseDoor(false);		
	}
	
}

void UOpenDoor::OpenCloseDoor(bool open)
{
	if (open) {
		Owner->SetActorRotation(FRotator(0, OpenAngle, 0));
		GEngine->AddOnScreenDebugMessage(-1, 0.5f, FColor::Cyan, "Open");
	}
	else {
		GEngine->AddOnScreenDebugMessage(-1, 0.5f, FColor::Red, "Close");
		Owner->SetActorRotation(FRotator(0, 0, 0));
	}
}

